import unittest
from hello_world import hello


class HelloTest(unittest.TestCase):
    def setUp(self):
        self.name = "Vasya"

    def test_type(self):
        self.assertEqual(type(hello()), str)

    def test_result(self):
        self.assertEqual(hello(name=self.name), "Hello Vasya")