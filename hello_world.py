
def hello(name='World'):
    return f"Hello {name}"


print(hello())
print(hello(name="Roman"))
